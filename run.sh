#!/bin/bash
IMAGE="node"
TAG="latest"

docker rm -f $IMAGE

docker run -tid --restart=always \
-e GIT_REPOSITORY=https://wiraperdana@bitbucket.org/bencooling/node-hello-world.git \
-e INDEX_FILE=server.js \
-p 19080:5000 \
--name $IMAGE \
anilornd/$IMAGE:$TAG
